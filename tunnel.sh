#!/bin/bash

#обновление
apt-get update && apt-get upgrade

#установка ngrok и токена 
curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | tee
              /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb
              https://ngrok-agent.s3.amazonaws.com buster main" | tee
              /etc/apt/sources.list.d/ngrok.list && sudo apt update && apt
              install ngrok

ngrok config add-authtoken 1piNoGoQyTVMIYfuSia7OfTh6NW_3vxEumCXtLJ7wSRxyo43J

#установка python3.8
apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget
curl -O https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tar.xz
tar -xf Python-3.8.2.tar.xz
cd Python-3.8.2
./configure --enable-optimizations
make -j 4
make altinstall

#скачивание репозитория
apt install unzip
cd /home
wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1nsfn_S-Ixb5LVmnrWtKio_SNgFdxvrRA' -O tunnel.zip
unzip tunnel.zip
cd tunnel_forwarder-master

#заполнение переменных
echo -e "SERVER_ENDPOINT=https://rpi.bsgmelio.ru/admin/service/exchange\nSERVER_TOKEN=6981ab74-9c69-4806-b4d5-bd64cd095c24\nUPDATE_INTERVAL=20\nLOG_PATH=./logs/log.log" > .env

#установка зависимостей
python3.8 -m pip install -r ./requirements.txt
python3.8 -m pip install python-dotenv[cli]

#создание демона тоннелей
echo -e "[Unit]\nDescription = Tunnel service\nAfter = network.target\n\n[Service]\nExecStart = /usr/local/bin/dotenv -f /home/tunnel_forwarder-master/.env run /usr/local/bin/python3.8 /home/tunnel_forwarder-master/main.py\nWorkingDirectory = /home/tunnel_forwarder-master/\nRestart = always\n\n[Install]\nWantedBy = multi-user.target" > /etc/systemd/system/tunnel_forwarder.service

#права и запуск
chmod 644 /etc/systemd/system/tunnel_forwarder.service
systemctl daemon-reload
systemctl enable tunnel_forwarder.service
systemctl start tunnel_forwarder
